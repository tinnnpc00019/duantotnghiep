package controller;

import java.util.List;

import javax.transaction.Transactional;

import org.apache.commons.codec.digest.DigestUtils;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import model.Nguoidung;

@Transactional
@Controller
@RequestMapping("/dangki/")
public class DangkiController {
	
	@Autowired
	SessionFactory factory;

	@RequestMapping("DangKi")
	public String index(ModelMap m, @ModelAttribute("allnguoidung") List<Nguoidung> nguoidung) {
		m.addAttribute("nguoidung", new Nguoidung());
		m.addAttribute("nguoidungs", nguoidung);
		return "admin/DangKi";
	}
	
	@ModelAttribute("allnguoidung")
	public List<Nguoidung> findAllNguoiDung() {
		Session s = factory.openSession();
		String hql = "From Nguoidung";
		Query query = s.createQuery(hql);
		List<Nguoidung> list = query.list();
		return list;
	}
	
	@RequestMapping("insert")
	public String insert(ModelMap model, @ModelAttribute("nguoidung") Nguoidung nguoidung,
			@ModelAttribute("allnguoidung") List<Nguoidung> nguoidungs, BindingResult errors) {
		Session session = factory.openSession();
		Transaction t = session.beginTransaction();
		try {
			String s = "ahihi";
			String rs = DigestUtils.sha256Hex(s);
			//String rs = DigestUtils.md5Hex(s)
			System.out.println(rs);
            nguoidung.setVaitro(false);
			session.save(nguoidung);
			t.commit();
			model.addAttribute("message", "Đăng kí thành công !");
			model.addAttribute("nguoidungs", findAllNguoiDung());

		} catch (Exception e) {
			t.rollback();
			model.addAttribute("message", "Đăng kí thất bại !");
			model.addAttribute("key", nguoidung.getSodt());
			model.addAttribute("table", "khách hàng");
			model.addAttribute("url", "nguoidung/ThemNguoiDung.htm");
			model.addAttribute("nguoidung", "số điện thoại hoặc email");
			model.addAttribute("nguoidungs", findAllNguoiDung());
//			return "admin/errorInsert";
		} finally {
			session.close();
		}
		return "admin/Login";
	}
}
