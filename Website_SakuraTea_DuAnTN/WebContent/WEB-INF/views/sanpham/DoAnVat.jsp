<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="sTinht"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<base href="${pageContext.servletContext.contextPath}/">
<base href="${pageContext.servletContext.contextPath}/">
<title>Sakura Tea</title>


<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous">

<!-- Font Awesome -->
<link href="admin/vendors/font-awesome/css/font-awesome.min.css"
	rel="stylesheet">

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
	integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
	crossorigin="anonymous"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
	integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
	crossorigin="anonymous"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
	integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
	crossorigin="anonymous"></script>
<link rel="stylesheet" href="css/giaodien.css">

<style type="text/css">
.footer {
  background: #3f4041;
  color: white;
}
.footer .links ul {
  list-style-type: none;
}
.footer .links li a {
  color: white;
  -webkit-transition: color .2s;
  transition: color .2s;
}
.footer .links li a:hover {
  text-decoration: none;
  color: #4180CB;
}
.footer .about-company i {
  font-size: 25px;
}
.footer .about-company a {
  color: white;
  -webkit-transition: color .2s;
  transition: color .2s;
}
.footer .about-company a:hover {
  color: #4180CB;
}
.footer .location i {
  font-size: 18px;
}

</style>
</head>
<body>
<div>Melem mlem</div>
	<!--Navigation-->
	<nav class=" navbar-expand-md navbar-light bg-light sticky-top">
		<div class="container-fluid">
			<a class="navbar-brand ml-5" href="#"><img
				src="./css/images/86c3807d1867e739be76.jpg" width="60vh"
				height="60vh"></a>
			<button class="navbar-toggler" type="button" data-toggle="collapse"
				data-target="#navbarCollapse" aria-expanded="false"
				aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarCollapse">
				<ul class="navbar-nav ml-5">
					<li class="nav-item active pd-nav"><a href="Home.htm" class="nav-link">TRANG
							CHỦ</a></li>
					<li class="nav-item pd-nav"><a href="" class="nav-link">GIỚI
							THIỆU</a></li>
					<li class="nav-item dropdown pd-nav">
      <a class="nav-link dropdown-toggle"  id="navbardrop" data-toggle="dropdown">
        THỨC UỐNG
      </a>
      <div  class="dropdown-menu" >
        <a class="dropdown-item" href="Home/ThucUong/TraSua.htm">TRÀ SỮA</a>
        <a class="dropdown-item" href="Home/ThucUong/TraTraiCay.htm">TRÀ TRÁI CÂY</a>
        <hr>
        <a class="dropdown-item pd-nav" href="Home/ThucUong/Topping.htm">TOPPING</a>
      </div>
    </li>
					<li class="nav-item pd-nav"><a href="Home/ThucUong/DoAnVat.htm" class="nav-link">ĐỒ ĂN
							VẶT</a></li>
					<li class="nav-item pd-nav"><a href="#" class="nav-link">HƯỚNG
							DẪN ĐẶT HÀNG</a></li>
					<li class="nav-item pd-nav"><a href="Home/LienHe.htm" class="nav-link">LIÊN HỆ</a></li>
					<li class="nav-item ml-5 pd-nav"><a href="#" class="nav-link">ĐĂNG
							NHẬP</a></li>
				</ul>

			</div>
		</div>
	</nav>

<div class="container" style="max-width: 90%; background: ;">
		
		<!-- Sản  Phẩm -->

		<!-- Đồ Ăn Vặt -->
		<div style="margin-top: 3vh">
			<h2 align="center">
				<b>ĐỒ ĂN VẶT</b>
			</h2>
			<hr>
			<c:forEach var="u" items="${loaddav}">
				<div class="card card-sp" align="center" style="">
					<img name="hinh" class="card-img-top image"
						src="admin/hinh/${u.hinh}" alt="Card image cap">

					<div class="card-body">
						<h6 name="tensp" class="card-title">${u.tensp}</h6>
						<p name="gia" class="card-text">${u.gia} VNĐ</p>
					</div>                 
					<a href="SanPham/${u.masp}.htm" class="btn effect01" >
					<span>CHI TIẾT</span>
					</a>
					<a href="SanPham/${u.masp}.htm" class="btn effect01">
					<span>+ GIỎ HÀNG</span>
					</a>					
				</div>
			</c:forEach>
			<br>
			<br> 
		
		</div>
		</div>


	<!-- Footer -->
<div class="mt-5 pt-5 pb-5 footer">
<div class="container">
  <div class="row">
    <div class="col-lg-5 col-xs-12 about-company">
      <h2>Heading</h2>
      <p class="pr-5 text-white-50">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam ac ante mollis quam tristique convallis </p>
      <p><a href="#"><i class="fa fa-facebook-square mr-1"></i></a><a href="#"><i class="fa fa-linkedin-square"></i></a></p>
    </div>
    <div class="col-lg-3 col-xs-12 links">
      <h4 class="mt-lg-0 mt-sm-3">Links</h4>
        <ul class="m-0 p-0">
          <li>- <a href="#">Lorem ipsum</a></li>
          <li>- <a href="#">Nam mauris velit</a></li>
          <li>- <a href="#">Etiam vitae mauris</a></li>
          <li>- <a href="#">Fusce scelerisque</a></li>
          <li>- <a href="#">Sed faucibus</a></li>
          <li>- <a href="#">Mauris efficitur nulla</a></li>
        </ul>
    </div>
    <div class="col-lg-4 col-xs-12 location">
      <h4 class="mt-lg-0 mt-sm-4">Location</h4>
      <p>22, Lorem ipsum dolor, consectetur adipiscing</p>
      <p class="mb-0"><i class="fa fa-phone mr-3"></i>(541) 754-3010</p>
      <p><i class="fa fa-envelope-o mr-3"></i>info@hsdf.com</p>
    </div>
  </div>
 
  <div class="row mt-5">
    <div class="col copyright">
      <p class=""><small class="text-white-50">© 2019. All Rights Reserved.</small></p>
    </div>
  </div>
</div>
</div>

	<!-- Footer -->
</body>
</html>