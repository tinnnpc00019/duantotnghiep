<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<base href="${pageContext.servletContext.contextPath}/">

<title>Sakura Tea</title>


<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous">

<!-- Font Awesome -->
<link href="admin/vendors/font-awesome/css/font-awesome.min.css"
	rel="stylesheet">

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
	integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
	crossorigin="anonymous"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
	integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
	crossorigin="anonymous"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
	integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
	crossorigin="anonymous"></script>


<style type="text/css">


.quantity-control {
  display: flex;
  	
  justify-content: space-between;
  width: fit-content;
  
  background: #eaeaea;
  border-radius: 10px;
 margin: -1.7rem 0px 0px 13rem;

}
.ex3{

  width: 100%;
  height: 120px;
  overflow: auto;
}
.quantity-btn {
  background: transparent;
  border: none;
  outline: none;
  margin: 0;
  padding: 0px 8px;
  cursor: pointer;
}
.quantity-btn svg {
  width: 15px;
  height: 15px;
}
.quantity-input {
  outline: none;
  user-select: none;
  text-align: center;
  width: 47px;
  display: flex;
  align-items: center;
  justify-content: center;
  background: transparent;
  border: none;
}
.quantity-input::-webkit-inner-spin-button,
.quantity-input::-webkit-outer-spin-button {
  -webkit-appearance: none;
  margin: 0;
}

</style>
<link rel="stylesheet" href="css/ChiTietSP.css">
</head>
<body>

	<!--Navigation-->
	<nav class="navbar navbar-expand-md navbar-light bg-light sticky-top">
		<div class="container-fluid">
			<a class="navbar-brand ml-5" href="#"><img
				src="./css/images/86c3807d1867e739be76.jpg" width="60vh"
				height="60vh"></a>
			<button class="navbar-toggler" type="button" data-toggle="collapse"
				data-target="#navbarCollapse" aria-expanded="false"
				aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarCollapse">
				<ul class="navbar-nav ml-5">
					<li class="nav-item active pd-nav"><a href="Home.htm"
						class="nav-link">TRANG CHỦ</a></li>
					<li class="nav-item pd-nav"><a href="" class="nav-link">GIỚI
							THIỆU</a></li>
					<li class="nav-item dropdown pd-nav"><a
						class="nav-link dropdown-toggle" id="navbardrop"
						data-toggle="dropdown"> THỨC UỐNG </a>
						<div class="dropdown-menu">
							<a class="dropdown-item" href="Home/ThucUong/TraSua.htm">TRÀ
								SỮA</a> <a class="dropdown-item" href="Home/ThucUong/TraTraiCay.htm">TRÀ
								TRÁI CÂY</a>
							<hr>
							<a class="dropdown-item pd-nav" href="Home/ThucUong/Topping.htm">TOPPING</a>
						</div></li>
					<li class="nav-item pd-nav"><a
						href="Home/ThucUong/DoAnVat.htm" class="nav-link">ĐỒ ĂN VẶT</a></li>
					<li class="nav-item pd-nav"><a href="#" class="nav-link">HƯỚNG
							DẪN ĐẶT HÀNG</a></li>
					<li class="nav-item pd-nav"><a href="Home/LienHe.htm"
						class="nav-link">LIÊN HỆ</a></li>
					<li class="nav-item ml-5 pd-nav"><a href="#" class="nav-link">ĐĂNG
							NHẬP</a></li>
				</ul>

			</div>
		</div>
	</nav>

	
	
	<!-- Page Content -->
<div class="container">
<form:form action="#" modelAttribute="Sanpham">
  <!-- Portfolio Item Heading -->
  <h1 class="my-4">ĐẶT HÀNG CHI TIẾT
  </h1>

  <!-- Portfolio Item Row -->
  <div class="row">

    <div class="col-md-7" align="center"style="border-right:1px solid lightgrey;">
      <img class="img-fluid class" width="550px" src="admin/hinh/${Sanpham.hinh}" alt="">
    </div>

    <div class="col-md-4">
      <h3 class="my-3">${Sanpham.tensp}</h3> <br>
     <h5>Giá: ${Sanpham.gia} VNĐ</h5> <br>
      
     
      <!-- Size -->
       <div ><h5 style="display:inline-block; margin-right:13rem " >Size</h5>
        <label class="radio-inline">
      <input type="radio" name="size" checked> M 
    </label>
    <label class="radio-inline">
      <input type="radio" name="size"> L
    </label>
      </div>
      <br>
      
      
      <!-- Quantity -->
      <h5>Số lượng: </h5>
     <div class="quantity-control" data-quantity="">
     <button class="quantity-btn" data-quantity-minus=""><svg viewBox="0 0 409.6 409.6">
        <g>
          <g>
            <path d="M392.533,187.733H17.067C7.641,187.733,0,195.374,0,204.8s7.641,17.067,17.067,17.067h375.467 c9.426,0,17.067-7.641,17.067-17.067S401.959,187.733,392.533,187.733z" />
          </g>
        </g>
      </svg></button>
    <input type="number" class="quantity-input" data-quantity-target="" value="1" step="1" min="1" max="" name="quantity">
    <button class="quantity-btn" data-quantity-plus=""><svg viewBox="0 0 426.66667 426.66667">
        <path d="m405.332031 192h-170.664062v-170.667969c0-11.773437-9.558594-21.332031-21.335938-21.332031-11.773437 0-21.332031 9.558594-21.332031 21.332031v170.667969h-170.667969c-11.773437 0-21.332031 9.558594-21.332031 21.332031 0 11.777344 9.558594 21.335938 21.332031 21.335938h170.667969v170.664062c0 11.777344 9.558594 21.335938 21.332031 21.335938 11.777344 0 21.335938-9.558594 21.335938-21.335938v-170.664062h170.664062c11.777344 0 21.335938-9.558594 21.335938-21.335938 0-11.773437-9.558594-21.332031-21.335938-21.332031zm0 0" /></svg>
    </button>
  </div>
      
      
      <!-- Ice -->
      <h5 class="my-3">Ice</h5>
      <div>
      
	<form action="#" method="GET">
	
		<div id="debt-amount-slider">
			<input type="radio" name="Ice" id="1" value="10" required>
			<label for="1" data-debt-amount="10%"></label>
			<input type="radio" name="Ice" id="2" value="20" required>
			<label for="2" data-debt-amount="30%"></label>
			<input type="radio" name="Ice" id="3" value="30" required>
			<label for="3" data-debt-amount="50%"></label>
			<input type="radio" name="Ice" id="4" value="40" required>
			<label for="4" data-debt-amount="70%"></label>
			<input type="radio" name="Ice" id="5" value="50" required>
			<label for="5" data-debt-amount="100%"></label>
			<div id="debt-amount-pos"></div>
		</div>
	</form>
      </div>
      
      
      <br>
            <!-- Sugar -->
      <h5 class="my-3">Sugar</h5>
      <div>
      
	
	
		<div id="debt-amount-slider">
			<input type="radio" name="Sugar" id="6" value="1" required>
			<label for="6" data-debt-amount="10%"></label>
			<input type="radio"  name="Sugar" id="7" value="2" required>
			<label for="7" data-debt-amount="30%"></label>
			<input type="radio"  name="Sugar" id="8" value="3" required>
			<label for="8" data-debt-amount="50%"></label>
			<input type="radio"  name="Sugar" id="9" value="4" required>
			<label for="9" data-debt-amount="70%"></label>
			<input type="radio"  name="Sugar" id="10" value="5" required>
			<label for="10" data-debt-amount="100%"></label>
			<div id="debt-amount-pos"></div>
		</div>
	<br>
	<br>
	<h5>Topping</h5>
	 
	  <div class="ex3">
	 
	  <c:forEach var="u" items="${loadtp}">
    <label class="checkbox" style="margin: 3px">
      <input type="checkbox" value="+ ${u.gia}: ${u.tensp}"> ${u.tensp}
    </label><br>
    
    </c:forEach>
 </div>
	
	<br>
	 <h5>Ghi Chú: </h5><textarea rows="3" cols="40" id="field_results"></textarea>
      </div>
     
     <br>
     <button class="btn btn-info" style="max-width: 500px">THÊM VÀO GIỎ HÀNG</button>
    </div>

<div>

</div>

  </div>
  <!-- /.row -->
</form:form>
  <!-- Related Projects Row -->
  <h3 class="my-4" style="border-bottom:0.5px solid grey;">CÁC LOẠI TRÀ SỮA KHÁC</h3>

  	<div class="container"
		style="margin-top: 3vh; max-width: 90%; ">
		<h3 >
		
		</h3>
		<c:forEach var="u" items="${loadts}">
			<div class="card card-sp" align="center" style="">
				<img name="hinh" class="card-img-top image"
					src="admin/hinh/${u.hinh}" alt="Card image cap">

				<div class="card-body">
					<h6 name="tensp" class="card-title">${u.tensp}</h6>
					<p name="gia" class="card-text">${u.gia}</p>
				</div>
				<a href="SanPham/${u.masp}.htm" class="btn effect01"> <span>CHI
						TIẾT</span>
				</a> <a href="SanPham/${u.masp}.htm" class="btn effect01"> <span>+
						GIỎ HÀNG</span>
				</a>
			</div>
		</c:forEach>	

	</div>
 

  </div>
  <!-- /.row -->

</div>
<!-- /.container -->
	
	
	
	<!-- Footer -->
	<div class="mt-5 pt-5 pb-5 footer">
		<div class="container">
			<div class="row">
				<div class="col-lg-5 col-xs-12 about-company">
					<h2>Heading</h2>
					<p class="pr-5 text-white-50">Lorem ipsum dolor sit amet,
						consectetur adipiscing elit. Nullam ac ante mollis quam tristique
						convallis</p>
					<p>
						<a href="#"><i class="fa fa-facebook-square mr-1"></i></a><a
							href="#"><i class="fa fa-linkedin-square"></i></a>
					</p>
				</div>
				<div class="col-lg-3 col-xs-12 links">
					<h4 class="mt-lg-0 mt-sm-3">Links</h4>
					<ul class="m-0 p-0">
						<li>- <a href="#">Lorem ipsum</a></li>
						<li>- <a href="#">Nam mauris velit</a></li>
						<li>- <a href="#">Etiam vitae mauris</a></li>
						<li>- <a href="#">Fusce scelerisque</a></li>
						<li>- <a href="#">Sed faucibus</a></li>
						<li>- <a href="#">Mauris efficitur nulla</a></li>
					</ul>
				</div>
				<div class="col-lg-4 col-xs-12 location">
					<h4 class="mt-lg-0 mt-sm-4">Location</h4>
					<p>22, Lorem ipsum dolor, consectetur adipiscing</p>
					<p class="mb-0">
						<i class="fa fa-phone mr-3"></i>(541) 754-3010
					</p>
					<p>
						<i class="fa fa-envelope-o mr-3"></i>info@hsdf.com
					</p>
				</div>
			</div>

			<div class="row mt-5">
				<div class="col copyright">
					<p class="">
						<small class="text-white-50">© 2019. All Rights Reserved.</small>
					</p>
				</div>
			</div>
		</div>
	</div>
	<!-- Footer -->
	
	
	
	
	

	
	
	<script type="text/javascript">
	
	$(document).ready(function(){
	    $checks = $(":checkbox");
	    $rdochecks = $(":radio");
	    $checks.on('change', function() {
	        var string = $checks.filter(":checked").map(function(i,v){
	            return this.value;
	        }).get().join(" ");
	        $('#field_results').val(string);
	    });
	    
	    
	});
	
	(function () {
		  "use strict";
		  var jQueryPlugin = (window.jQueryPlugin = function (ident, func) {
		    return function (arg) {
		      if (this.length > 1) {
		        this.each(function () {
		          var $this = $(this);

		          if (!$this.data(ident)) {
		            $this.data(ident, func($this, arg));
		          }
		        });

		        return this;
		      } else if (this.length === 1) {
		        if (!this.data(ident)) {
		          this.data(ident, func(this, arg));
		        }

		        return this.data(ident);
		      }
		    };
		  });
		})();

		(function () {
		  "use strict";
		  function Guantity($root) {
		    const element = $root;
		    const quantity = $root.first("data-quantity");
		    const quantity_target = $root.find("[data-quantity-target]");
		    const quantity_minus = $root.find("[data-quantity-minus]");
		    const quantity_plus = $root.find("[data-quantity-plus]");
		    var quantity_ = quantity_target.val();
		    $(quantity_minus).click(function () {
		      quantity_target.val(--quantity_);
		    });
		    $(quantity_plus).click(function () {
		      quantity_target.val(++quantity_);
		    });
		  }
		  $.fn.Guantity = jQueryPlugin("Guantity", Guantity);
		  $("[data-quantity]").Guantity();
		})();

	
	</script>
	
</body>
</html>