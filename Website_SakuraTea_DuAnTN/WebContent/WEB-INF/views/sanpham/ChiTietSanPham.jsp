<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<base href="${pageContext.servletContext.contextPath}/">

<title>Sakura Tea</title>


<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous">

<!-- Font Awesome -->
<link href="admin/vendors/font-awesome/css/font-awesome.min.css"
	rel="stylesheet">

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
	integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
	crossorigin="anonymous"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
	integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
	crossorigin="anonymous"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
	integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
	crossorigin="anonymous"></script>
<link rel="stylesheet" href="css/giaodien.css">

</head>
<body>

	<!--Navigation-->
	<nav class="navbar navbar-expand-md navbar-light bg-light sticky-top">
		<div class="container-fluid">
			<a class="navbar-brand ml-5" href="#"><img
				src="./css/images/86c3807d1867e739be76.jpg" width="60vh"
				height="60vh"></a>
			<button class="navbar-toggler" type="button" data-toggle="collapse"
				data-target="#navbarCollapse" aria-expanded="false"
				aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarCollapse">
				<ul class="navbar-nav ml-5">
					<li class="nav-item active pd-nav"><a href="Home.htm" class="nav-link">TRANG
							CHỦ</a></li>
					<li class="nav-item pd-nav"><a href="" class="nav-link">GIỚI
							THIỆU</a></li>
					<li class="nav-item dropdown pd-nav">
      <a class="nav-link dropdown-toggle"  id="navbardrop" data-toggle="dropdown">
        THỨC UỐNG
      </a>
      <div  class="dropdown-menu" >
        <a class="dropdown-item" href="Home/ThucUong/TraSua.htm">TRÀ SỮA</a>
        <a class="dropdown-item" href="Home/ThucUong/TraTraiCay.htm">TRÀ TRÁI CÂY</a>
        <hr>
        <a class="dropdown-item pd-nav" href="Home/ThucUong/Topping.htm">TOPPING</a>
      </div>
    </li>
					<li class="nav-item pd-nav"><a href="Home/ThucUong/DoAnVat.htm" class="nav-link">ĐỒ ĂN
							VẶT</a></li>
					<li class="nav-item pd-nav"><a href="#" class="nav-link">HƯỚNG
							DẪN ĐẶT HÀNG</a></li>
					<li class="nav-item pd-nav"><a href="Home/LienHe.htm" class="nav-link">LIÊN HỆ</a></li>
					<li class="nav-item ml-5 pd-nav"><a href="#" class="nav-link">ĐĂNG
							NHẬP</a></li>
				</ul>

			</div>
		</div>
	</nav>



<form:form action="#" modelAttribute="Sanpham">

			<div class="card"
				style="border: none; width: 14rem; margin-bottom: 3vh; display: inline-block; padding: 10px; margin-left: 3px">
				<img class="card-img-top" width="300px"
					src="admin/hinh/${Sanpham.hinh}" alt="Card image cap">			
					
				<div class="card-body">
					<h6 class="card-title">${Sanpham.tensp}</h6>
					<p class="card-text">${Sanpham.gia}</p>
				</div>
				
			</div>


</form:form>

<br><br><br>
<hr>
		<h3>
			<b>TRÀ SỮA</b>
		</h3>
		<hr>

		<c:forEach var="u" items="${loadts}">


			<div class="card"
				style="border: none; width: 14rem; margin-bottom: 3vh; display: inline-block; padding: 10px; margin-left: 3px">
				<img class="card-img-top" 
					src="admin/hinh/${u.hinh}" width="300px" alt="Card image cap">			
					<a href="SanPham/${u.masp}.htm"><button class="btn btn-info btnChiTiet" >Chi
					tiết</button></a>
				<div class="card-body">
					<h6 class="card-title">${u.tensp}</h6>
					<p class="card-text">${u.gia}</p>
				</div>
				
			</div>


		</c:forEach> 



<!-- Footer -->
<footer class="page-footer font-small teal pt-4" style="background:lightpink; color: white;">

  <!-- Footer Text -->
  <div class="container-fluid text-center text-md-left">

    <!-- Grid row -->
    <div class="row">

      <!-- Grid column -->
      <div class="col-md-6 mt-md-0 mt-3">

        <!-- Content -->
        <h5 class="text-uppercase font-weight-bold">Footer text 1</h5>
        <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Expedita sapiente sint, nulla, nihil
          repudiandae commodi voluptatibus corrupti animi sequi aliquid magnam debitis, maxime quam recusandae
          harum esse fugiat. Itaque, culpa?</p>

      </div>
      <!-- Grid column -->

      <hr class="clearfix w-100 d-md-none pb-3">

      <!-- Grid column -->
      <div class="col-md-6 mb-md-0 mb-3">

        <!-- Content -->
        <h5 class="text-uppercase font-weight-bold">Footer text 2</h5>
        <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Optio deserunt fuga perferendis modi earum
          commodi aperiam temporibus quod nulla nesciunt aliquid debitis ullam omnis quos ipsam, aspernatur id
          excepturi hic.</p>

      </div>
      <!-- Grid column -->

    </div>
    <!-- Grid row -->

  </div>
  <!-- Footer Text -->

  <!-- Copyright -->
  <hr style="background: white;">
  <div class="footer-copyright text-center py-3">© 2020 Copyright:
    <a href="#"> Sakura Tea</a>
  </div>
  <!-- Copyright -->

</footer>
<!-- Footer -->
</body>
</html>