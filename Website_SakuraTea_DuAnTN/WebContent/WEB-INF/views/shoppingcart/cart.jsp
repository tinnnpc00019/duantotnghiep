<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="sTinht"%>
<!DOCTYPE html>
<html>
<head>
<base href="${pageContext.servletContext.contextPath}/">
<meta charset="UTF-8">
<title>Insert title here</title>

</head>
<body>
<div align="center">
<h1>Spring MVC And Hibernate Shopping Cart</h1>
<table border="2" >
<tr>
<th>Options</th>
<th>STT</th>
<th>Ma Item</th>
<th >Ma SP</th>
<th>Photo</th>
<th >Ten SP</th>
<th >Gia</th>
<th >So Luong</th>
<th >Sub Total</th>

 
</tr>

<c:set var="s" value="0"></c:set>
<c:forEach var="pr" items="${sessionScope.cart}">
<c:set var="s"  value="${s+ pr.sp.gia * pr.quantity}"></c:set>
<tr>
<td><a href="delete/${pr.stt }.htm">Remove </a> </td>
<td>${pr.stt }</td>
<td>${pr.maItem }</td>
<td>${pr.sp.masp }</td>

<td>
<img alt="" src="admin/hinh/${pr.sp.hinh}" height="100px" width="100px">

</td>

<td>${pr.sp.tensp}</td>
<td>${pr.sp.gia }</td>
<td>${pr.quantity }</td>
<td>${pr.sp.gia * pr.quantity}</td>
</tr>
  
</c:forEach>

<tr><td colspan="6" align="right">Sum</td>
<td>${s}</td>

</tr>
</table>
 <a href="index.htm">Shoping</a>  <br> 

</div>

</body>
</html>