<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="sTinht"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<base href="${pageContext.servletContext.contextPath}/">
<title>Sakura Tea</title>


<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous">

<!-- Font Awesome -->
<link href="admin/vendors/font-awesome/css/font-awesome.min.css"
	rel="stylesheet">

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
	integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
	crossorigin="anonymous"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
	integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
	crossorigin="anonymous"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
	integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
	crossorigin="anonymous"></script>
<link rel="stylesheet" href="css/giaodien.css">

<style type="text/css">
.card-sp {
	border: none;
	width: 18rem;
	
	margin-bottom: 3vh;
	display: inline-block;
	padding: 10px;
	margin: 0vh 0.6vh 3vh 0.6vh
}

.image{
height: 40vh;
}

.btn {
	letter-spacing: 0.1em;
	cursor: pointer;
	font-size: 12px;
	font-weight: 700;
	line-height: 30px;
	max-width: 130px;
	position: relative;
	text-decoration: none;
	text-transform: uppercase;
	width: 100%;
}

.btn-xemthem {
    font-family: Hack, monospace;
    background: #0F0F6D;
    color: #ffffff;
    cursor: pointer;
    font-size: 2em;
    padding: 1.5rem;
    border: 0;
    transition: all 0.5s;
    border-radius: 10px;
    width: auto;
    position: relative;

    &::after {
        content: "\f054";
        font-family: "Font Awesome 5 Pro";
        font-weight: 400;
        position: absolute;
        left: 85%;
        top: 31%;
        right: 5%;
        bottom: 0;
        opacity: 0;

    }

    &:hover {
        background: #2b2bff;
        transition: all 0.5s;
        border-radius: 10px;
        box-shadow: 0px 6px 15px #0000ff61;
        padding: 1.5rem 3.5rem 1.5rem 1.5rem;

        &::after {
            opacity: 1;
            transition: all 0.5s;

        }
    }


}


.btn:hover {
	text-decoration: none;
}

/*btn_background*/
.effect01 {
	color: #000;
	border: 4px solid #FFF;
	box-shadow: 0px 0px 0px 1px #000 inset;
	background-color: #FFF;
	overflow: hidden;
	position: relative;
	transition: all 0.3s ease-in-out;
}

.effect01:hover {
	border: 4px solid #666;
	background-color: #000;
	box-shadow: 0px 0px 0px 4px #EEE inset;
}

/*btn_text*/
.effect01 span {
	transition: all 0.2s ease-out;
	z-index: 2;
}

.effect01:hover span {
	letter-spacing: 0.13em;
	color: #FFF;
}

/*highlight*/
.effect01:after {
	background: #FFF;
	border: 0px solid #000;
	content: "";
	height: 125px;
	left: -75px;
	opacity: .8;
	position: absolute;
	top: -50px;
	-webkit-transform: rotate(35deg);
	transform: rotate(35deg);
	width: 50px;
	transition: all 1s cubic-bezier(0.075, 0.82, 0.165, 1); /*easeOutCirc*/
	z-index: 1;
}

.effect01:hover:after {
	background: #FFF;
	border: 20px solid #000;
	opacity: 0;
	left: 120%;
	-webkit-transform: rotate(40deg);
	transform: rotate(40deg);
}



.footer {
  background: #3f4041;
  color: white;
}
.footer .links ul {
  list-style-type: none;
}
.footer .links li a {
  color: white;
  -webkit-transition: color .2s;
  transition: color .2s;
}
.footer .links li a:hover {
  text-decoration: none;
  color: #4180CB;
}
.footer .about-company i {
  font-size: 25px;
}
.footer .about-company a {
  color: white;
  -webkit-transition: color .2s;
  transition: color .2s;
}
.footer .about-company a:hover {
  color: #4180CB;
}
.footer .location i {
  font-size: 18px;
}
.footer .copyright p {
  border-top: 1px solid rgba(255, 255, 255, 0.1);
}

</style>

</head>
<body>

	<!--Navigation-->
	<nav class="navbar navbar-expand-md navbar-light bg-light sticky-top">
		<div class="container-fluid">
			<a class="navbar-brand ml-5" href="#"><img
				src="./css/images/86c3807d1867e739be76.jpg" width="60vh"
				height="60vh"></a>
			<button class="navbar-toggler" type="button" data-toggle="collapse"
				data-target="#navbarCollapse" aria-expanded="false"
				aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarCollapse">
				<ul class="navbar-nav ml-5">
					<li class="nav-item active pd-nav"><a href="Home.htm"
						class="nav-link">TRANG CHỦ</a></li>
					<li class="nav-item pd-nav"><a href="" class="nav-link">GIỚI
							THIỆU</a></li>
					<li class="nav-item dropdown pd-nav"><a
						class="nav-link dropdown-toggle" id="navbardrop"
						data-toggle="dropdown"> THỨC UỐNG </a>
						<div class="dropdown-menu">
							<a class="dropdown-item" href="Home/ThucUong/TraSua.htm">TRÀ
								SỮA</a> <a class="dropdown-item" href="Home/ThucUong/TraTraiCay.htm">TRÀ
								TRÁI CÂY</a>
							<hr>
							<a class="dropdown-item pd-nav" href="Home/ThucUong/Topping.htm">TOPPING</a>
						</div></li>
					<li class="nav-item pd-nav"><a
						href="Home/ThucUong/DoAnVat.htm" class="nav-link">ĐỒ ĂN VẶT</a></li>
					<li class="nav-item pd-nav"><a href="#" class="nav-link">HƯỚNG
							DẪN ĐẶT HÀNG</a></li>
					<li class="nav-item pd-nav"><a href="Home/LienHe.htm"
						class="nav-link">LIÊN HỆ</a></li>
					<li class="nav-item ml-5 pd-nav"><c:set var="dauhieu" value="${sessionScope.DAUHIEU}"/>
                            <c:if test="${dauhieu == 'dn'}">
                                
                            <li><a href="#">Xin chào ${hoten}</a></li>
                            <li><a href="HomeLogout.htm"><i class="fa fa-user-o"></i>/Đăng xuất</a></li>
                            </c:if>
                            <c:if test="${dauhieu == null}">
                            <li><a href="admin/Login.htm" id="DN"><i class="fa fa-user-o"></i>Đăng nhập</a></li>
                            </c:if>
               
							 </li>
					
							<li class="nav-item ml-5 pd-nav"><a href="Home.htm" class="nav-link"><img alt="shopping cart" style="margin-top: -8px" width="32" height="32" src="admin/hinh/shopping-cart.png"></a></li>
				</ul>

			</div>
		</div>
	</nav>

	<!--Carousel-->
	<div id="carouselExampleFade" class="carousel slide carousel-fade"
		data-ride="carousel">
		<div class="carousel-inner">
			<!-- <div class="carousel-item active">
				<img src="./css/images/banner1.jpg" class="d-block w-100 banner"
					alt="...">
			</div> -->
			<div class="carousel-item active">
				<img src="./css/images/banner2.png" class="d-block w-100 banner"
					alt="...">
			</div>
			<div class="carousel-item">
				<img src="./css/images/banner3.png" class="d-block w-100 banner"
					alt="...">
			</div>
		</div>
		<a class="carousel-control-prev" href="#carouselExampleFade"
			role="button" data-slide="prev"> <span
			class="carousel-control-prev-icon" aria-hidden="true"></span> <span
			class="sr-only">Previous</span>
		</a> <a class="carousel-control-next" href="#carouselExampleFade"
			role="button" data-slide="next"> <span
			class="carousel-control-next-icon" aria-hidden="true"></span> <span
			class="sr-only">Next</span>
		</a>
	</div>
<div class="card-img">
			<img src="./css/images/qc.jpg" class="d-block w-100" alt="...">
		</div>
	<!-- container -->
	<div class="container" style="max-width: 90%; background: ;">
		
		<!-- Sản  Phẩm -->

		<!-- Trà Sữa -->
		<div style="margin-top: 3vh">
			<h3 align="center">
				<b>TRÀ SỮA</b>
			</h3>
			<hr>
			<c:forEach var="u" items="${loadts}">
				<div class="card card-sp" align="center" style="">
					<img name="hinh" class="card-img-top image"
						src="admin/hinh/${u.hinh}" alt="Card image cap">

					<div class="card-body">
						<h6 name="tensp" class="card-title">${u.tensp}</h6>
						<p name="gia" class="card-text">${u.gia} VNĐ</p>
					</div>                 
					<a href="SanPham/${u.masp}.htm" class="btn effect01" >
					<span>CHI TIẾT</span>
					</a>
					<a href="SanPham/${u.masp}.htm" class="btn effect01">
					<span>+ GIỎ HÀNG</span>
					</a>					
				</div>
			</c:forEach>
			<br>
			<br> 
			<div align="center">
			<a href="Home/ThucUong/TraSua.htm" class="effect01 btn"
			 style="margin-bottom: 6vh">
			 <span>XEM THÊM</span>
			 </a>
			 </div>
		</div>
		
		

				<!-- Trà Trái Cây -->
		<div style="margin-top: 3vh">
			<h3 align="center">
				<b>TRÀ TRÁI CÂY</b>
			</h3>
			<hr>
			<c:forEach var="u" items="${loadttc}">
				<div class="card card-sp" align="center" style="">
					<img name="hinh" class="card-img-top image"
						src="admin/hinh/${u.hinh}" alt="Card image cap">

					<div class="card-body">
						<h6 name="tensp" class="card-title">${u.tensp}</h6>
						<p name="gia" class="card-text">${u.gia} VNĐ</p>
					</div>                 
					<a href="SanPham/${u.masp}.htm" class="btn effect01" >
					<span>CHI TIẾT</span>
					</a>
					<a href="SanPham/${u.masp}.htm" class="btn effect01">
					<span>+ GIỎ HÀNG</span>
					</a>					
				</div>
			</c:forEach>
			<br>
			<br> 
			<div align="center">
			<a href="Home/ThucUong/TraTraiCay.htm" class="effect01 btn"
			 style="margin-bottom: 6vh">
			 <span>XEM THÊM</span>
			 </a>
			 </div>
		</div>



				<!-- Đồ Ăn Vặt -->
		<div style="margin-top: 3vh">
			<h3 align="center">
				<b>ĐỒ ĂN VẶT</b>
			</h3>
			<hr>
			<c:forEach var="u" items="${loaddav}">
				<div class="card card-sp" align="center" style="">
					<img name="hinh" class="card-img-top image"
						src="admin/hinh/${u.hinh}" alt="Card image cap">

					<div class="card-body">
						<h6 name="tensp" class="card-title">${u.tensp}</h6>
						<p name="gia" class="card-text">${u.gia} VNĐ</p>
					</div>                 
					<a href="SanPham/${u.masp}.htm" class="btn effect01" >
					<span>CHI TIẾT</span>
					</a>
					<a href="SanPham/${u.masp}.htm" class="btn effect01">
					<span>+ GIỎ HÀNG</span>
					</a>					
				</div>
			</c:forEach>
			<br>
			<br> 
			<div align="center">
			<a href="Home/DoAnVat.htm" class="effect01 btn"
			 style="margin-bottom: 6vh">
			 <span>XEM THÊM</span>
			 </a>
			 </div>
		</div>

		<!-- Topping -->
		<div style="margin-top: 3vh">
			<h3 align="center">
				<b>TOPPING</b>
			</h3>
			<hr>
			<c:forEach var="u" items="${loadtp}">
				<div class="card card-sp" align="center" style="">
					<img name="hinh" class="card-img-top image" 
						src="admin/hinh/${u.hinh}" alt="Card image cap">

					<div class="card-body">
						<h6 name="tensp" class="card-title">${u.tensp}</h6>
						<p name="gia" class="card-text">${u.gia} VNĐ</p>
					</div>                 
					
				</div>
			</c:forEach>
			<br>
			<br> 
			<div align="center">
			<a href="Home/ThucUong/Topping.htm" class="effect01 btn"
			 style="margin-bottom: 6vh">
			 <span>XEM THÊM</span>
			 </a>
			 </div>
		</div>



	</div>

	<!-- Footer -->
<div class="mt-5 pt-5 pb-5 footer">
<div class="container">
  <div class="row">
    <div class="col-lg-5 col-xs-12 about-company">
      <h2>Heading</h2>
      <p class="pr-5 text-white-50">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam ac ante mollis quam tristique convallis </p>
      <p><a href="#"><i class="fa fa-facebook-square mr-1"></i></a><a href="#"><i class="fa fa-linkedin-square"></i></a></p>
    </div>
    <div class="col-lg-3 col-xs-12 links">
      <h4 class="mt-lg-0 mt-sm-3">Links</h4>
        <ul class="m-0 p-0">
          <li>- <a href="#">Lorem ipsum</a></li>
          <li>- <a href="#">Nam mauris velit</a></li>
          <li>- <a href="#">Etiam vitae mauris</a></li>
          <li>- <a href="#">Fusce scelerisque</a></li>
          <li>- <a href="#">Sed faucibus</a></li>
          <li>- <a href="#">Mauris efficitur nulla</a></li>
        </ul>
    </div>
    <div class="col-lg-4 col-xs-12 location">
      <h4 class="mt-lg-0 mt-sm-4">Location</h4>
      <p>22, Lorem ipsum dolor, consectetur adipiscing</p>
      <p class="mb-0"><i class="fa fa-phone mr-3"></i>(541) 754-3010</p>
      <p><i class="fa fa-envelope-o mr-3"></i>info@hsdf.com</p>
    </div>
  </div>
 
  <div class="row mt-5">
    <div class="col copyright">
      <p class=""><small class="text-white-50">© 2019. All Rights Reserved.</small></p>
    </div>
  </div>
</div>
</div>

	<!-- Footer -->
</body>
</html>