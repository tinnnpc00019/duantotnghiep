<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<base href="${pageContext.servletContext.contextPath}/">
<title>Phản hồi</title>

<!-- <link
	href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css"
	rel="stylesheet" id="bootstrap-css">
<script
	src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script> -->
	
	<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous">

<!-- Font Awesome -->
<link href="admin/vendors/font-awesome/css/font-awesome.min.css"
	rel="stylesheet">

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
	integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
	crossorigin="anonymous"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
	integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
	crossorigin="anonymous"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
	integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
	crossorigin="anonymous"></script>

<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<link rel="stylesheet" href="css/giaodien.css">

<style type="text/css">
.footer {
  background: #3f4041;
  color: white;
}
.footer .links ul {
  list-style-type: none;
}
.footer .links li a {
  color: white;
  -webkit-transition: color .2s;
  transition: color .2s;
}
.footer .links li a:hover {
  text-decoration: none;
  color: #4180CB;
}
.footer .about-company i {
  font-size: 25px;
}
.footer .about-company a {
  color: white;
  -webkit-transition: color .2s;
  transition: color .2s;
}
.footer .about-company a:hover {
  color: #4180CB;
}
.footer .location i {
  font-size: 18px;
}
.footer .copyright p {
  border-top: 1px solid rgba(255, 255, 255, 0.1);
}

</style>
</head>
<body>
<!--Navigation-->
	<nav class="navbar navbar-expand-md navbar-light bg-light sticky-top">
		<div class="container-fluid">
			<a class="navbar-brand ml-5" href="#"><img
				src="./css/images/86c3807d1867e739be76.jpg" width="60vh"
				height="60vh"></a>
			<button class="navbar-toggler" type="button" data-toggle="collapse"
				data-target="#navbarCollapse" aria-expanded="false"
				aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarCollapse">
				<ul class="navbar-nav ml-5">
					<li class="nav-item active pd-nav"><a href="Home.htm" class="nav-link">TRANG
							CHỦ</a></li>
					<li class="nav-item pd-nav"><a href="" class="nav-link">GIỚI
							THIỆU</a></li>
					<li class="nav-item dropdown pd-nav">
      <a class="nav-link dropdown-toggle"  id="navbardrop" data-toggle="dropdown">
        THỨC UỐNG
      </a>
      <div  class="dropdown-menu" >
        <a class="dropdown-item" href="Home/ThucUong/TraSua.htm">TRÀ SỮA</a>
        <a class="dropdown-item" href="Home/ThucUong/TraTraiCay.htm">TRÀ TRÁI CÂY</a>
        <hr>
        <a class="dropdown-item pd-nav" href="Home/ThucUong/Topping.htm">TOPPING</a>
      </div>
    </li>
					<li class="nav-item pd-nav"><a href="Home/ThucUong/DoAnVat.htm" class="nav-link">ĐỒ ĂN
							VẶT</a></li>
					<li class="nav-item pd-nav"><a href="#" class="nav-link">HƯỚNG
							DẪN ĐẶT HÀNG</a></li>
					<li class="nav-item pd-nav"><a href="Home/LienHe.htm" class="nav-link">LIÊN HỆ</a></li>
					<li class="nav-item ml-5 pd-nav"><a href="#" class="nav-link">ĐĂNG
							NHẬP</a>${username}</li>
				</ul>

			</div>
		</div>
	</nav>




	<section id="contact">
		<div class="container">
			<div class="well well-sm">
				<h3>
					
				</h3>
			</div>

			<div class="row" class="phai">
				<div class="col-md-5" style="">
					<h2 >THÔNG TIN LIÊN HỆ</h2>
					<p class="top">Địa chỉ:</p>
					<p>Hotline:</p>
					<p>Zalo:</p>
					<p>Email:</p>
					<br>
					<h2>LIÊN HỆ VỚI CHÚNG TÔI</h2>
					<form:form class="top" action="home/insert.htm" modelAttribute="phanhoi">
						<div class="form-group">
							<label>Họ tên:</label>
							<form:input class="form-control" value="" placeholder="Họ tên" path="hoten" />
						</div>
						<div class="form-group">
							<label>Email:</label>
							<form:input class="form-control" value="" path="email"
								placeholder="E-mail" />
						</div>
						<div class="form-group">
							<label>Nội dung:</label>
							<form:textarea rows="7" placeholder="Nội dung" value="" class="form-control"
								path="noidung" />
						</div>
						<button class="btn btn-danger" type="submit" name="button">
							 <i class="fa fa-paper-plane-o" aria-hidden="true"></i> Gửi</button>
					</form:form>
				</div>

				<div class="col-md-7 map">
					<iframe
						src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3928.8791336022027!2d105.75516411411117!3d10.026831675324479!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31a088476bb00027%3A0xd3c02747d1cbc8c6!2zMjg4IMSQxrDhu51uZyBOZ3V54buFbiBWxINuIExpbmgsIEjGsG5nIEzhu6NpLCBOaW5oIEtp4buBdSwgQ-G6p24gVGjGoSwgVmnhu4d0IE5hbQ!5e0!3m2!1svi!2s!4v1603435774106!5m2!1svi!2s"
						width="100%" height="742" frameborder="0" style="border: 0"allowfullscreen"></iframe>

				</div>
			</div>
		</div>
	</section>



	<!-- Footer -->
<div class="mt-5 pt-5 pb-5 footer">
<div class="container">
  <div class="row">
    <div class="col-lg-5 col-xs-12 about-company">
      <h2>Heading</h2>
      <p class="pr-5 text-white-50">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam ac ante mollis quam tristique convallis </p>
      <p><a href="#"><i class="fa fa-facebook-square mr-1"></i></a><a href="#"><i class="fa fa-linkedin-square"></i></a></p>
    </div>
    <div class="col-lg-3 col-xs-12 links">
      <h4 class="mt-lg-0 mt-sm-3">Links</h4>
        <ul class="m-0 p-0">
          <li>- <a href="#">Lorem ipsum</a></li>
          <li>- <a href="#">Nam mauris velit</a></li>
          <li>- <a href="#">Etiam vitae mauris</a></li>
          <li>- <a href="#">Fusce scelerisque</a></li>
          <li>- <a href="#">Sed faucibus</a></li>
          <li>- <a href="#">Mauris efficitur nulla</a></li>
        </ul>
    </div>
    <div class="col-lg-4 col-xs-12 location">
      <h4 class="mt-lg-0 mt-sm-4">Location</h4>
      <p>22, Lorem ipsum dolor, consectetur adipiscing</p>
      <p class="mb-0"><i class="fa fa-phone mr-3"></i>(541) 754-3010</p>
      <p><i class="fa fa-envelope-o mr-3"></i>info@hsdf.com</p>
    </div>
  </div>
 
  <div class="row mt-5">
    <div class="col copyright">
      <p class=""><small class="text-white-50">© 2019. All Rights Reserved.</small></p>
    </div>
  </div>
</div>
</div>

	<!-- Footer -->
</body>
</html>