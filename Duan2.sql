﻿create database DuAn2
use DuAn2
create table LoaiSanPham
(
	MaLoai nvarchar(10) primary key,
	TenLoai nvarchar(50)
)
create table SanPham
(
	MaSP nvarchar(10) primary key not null,
	TenSP nvarchar(50),
	Gia money default 0,
	MoTa nvarchar(50),
	Hinh nvarchar(50),
	LoaiId nvarchar(10) foreign key references LoaiSanPham (MaLoai)
)

create table NguoiDung
(
	MaND int identity(1, 1) primary key,
	MatKhau varchar(30) not null,
	HoTen nvarchar(100),
	SoDT varchar(11) unique not null,
	Email varchar(250) unique,
	DiaChi nvarchar(50),
	VaiTro bit
)

create table DonDatHang
(
	MaDH int identity(1, 1) primary key,
	HoTen nvarchar(100),
	SoDT varchar(10) not null,
	DiaChi nvarchar(50),
	NgayDat date ,
	NgayGiao date,
	TinhTrang bit,
	GhiChu nvarchar(250),
	TongTien float
)
create table DonDatHang_CT
(
	MaDHCT int identity(1, 1) primary key,
	MaDH int foreign key references DonDatHang (MaDH),
	MaSP nvarchar(10) foreign key references SanPham (MaSP),
	GhiChu nvarchar(250),
	SoLuong int default 0,
	TongGia int default 0,
	
)
create table PhanHoi
(
MaPH int identity(1, 1) primary key,
HoTen nvarchar(250),
Email varchar(250),
Ngay date,
TrangThai bit,
NoiDung nvarchar(250)
)


INSERT [dbo].[LoaiSanPham] ([MaLoai], [TenLoai]) VALUES (N'AV', N'Đồ ăn vặt')
INSERT [dbo].[LoaiSanPham] ([MaLoai], [TenLoai]) VALUES (N'FT', N'Trà trái cây')
INSERT [dbo].[LoaiSanPham] ([MaLoai], [TenLoai]) VALUES (N'TP', N'Topping')
INSERT [dbo].[LoaiSanPham] ([MaLoai], [TenLoai]) VALUES (N'TS', N'Trà sữa')
SET IDENTITY_INSERT [dbo].[NguoiDung] ON 

INSERT [dbo].[NguoiDung] ([MaND], [MatKhau], [HoTen], [SoDT], [Email], [DiaChi], [VaiTro]) VALUES (23, N'1', N'Nguyễn Ngọc Tín ', N'1', N'tinnnpc00019@fpt.edu.vn', N'An Giang', 1)
INSERT [dbo].[NguoiDung] ([MaND], [MatKhau], [HoTen], [SoDT], [Email], [DiaChi], [VaiTro]) VALUES (24, N'123', N'Nguyen Thuy Vi', N'asdasdasd', N'vinnpc00019@fpt.edu.vn', N'An Giang', 1)
INSERT [dbo].[NguoiDung] ([MaND], [MatKhau], [HoTen], [SoDT], [Email], [DiaChi], [VaiTro]) VALUES (26, N'123', N'Huỳnh Trung Tín', N'123', N'tinhtpc00153@fpt.edu.vn', N'An Giang', 0)
SET IDENTITY_INSERT [dbo].[NguoiDung] OFF
SET IDENTITY_INSERT [dbo].[PhanHoi] ON 

INSERT [dbo].[PhanHoi] ([MaPH], [HoTen], [Email], [Ngay], [TrangThai], [NoiDung]) VALUES (1, N'Nguyễn Ngọc Tín ', N'tinnnpc00019@fpt.edu.vn', CAST(0xC5410B00 AS Date), 0, N'asdasdas ')
INSERT [dbo].[PhanHoi] ([MaPH], [HoTen], [Email], [Ngay], [TrangThai], [NoiDung]) VALUES (2, N'Huuy', N'nntin20@gmail.com', CAST(0x12410B00 AS Date), 1, N'asdasd ')
INSERT [dbo].[PhanHoi] ([MaPH], [HoTen], [Email], [Ngay], [TrangThai], [NoiDung]) VALUES (3, N'Nguyễn Ngọc Tín ', N'tinnnpc00019@fpt.edu.vn', CAST(0xC5410B00 AS Date), 0, N'adasdasd')
SET IDENTITY_INSERT [dbo].[PhanHoi] OFF
INSERT [dbo].[SanPham] ([MaSP], [TenSP], [Gia], [MoTa], [Hinh], [LoaiId]) VALUES (N'AV01', N'Rong biển cháy tỏi', 60000.0000, N'Perfect ', N'c42bb4e0195649277fe1eaeab5411a05.jpg', N'AV')
INSERT [dbo].[SanPham] ([MaSP], [TenSP], [Gia], [MoTa], [Hinh], [LoaiId]) VALUES (N'AV02', N'Mực rim me', 60000.0000, N'Perfect x2', N'muc-rim-me.jpg', N'AV')
INSERT [dbo].[SanPham] ([MaSP], [TenSP], [Gia], [MoTa], [Hinh], [LoaiId]) VALUES (N'AV03', N'Da heo cháy tỏi', 60000.0000, N'Perfect ', N'daheochaytoi.jpg', N'AV')
INSERT [dbo].[SanPham] ([MaSP], [TenSP], [Gia], [MoTa], [Hinh], [LoaiId]) VALUES (N'AV04', N'Khô gà xé cay', 60000.0000, N'Perfect ', N'kho-ga.jpg', N'AV')
INSERT [dbo].[SanPham] ([MaSP], [TenSP], [Gia], [MoTa], [Hinh], [LoaiId]) VALUES (N'AV05', N'Khô bò sợi', 60000.0000, N'Perfect x2', N'kho-bo.jpg', N'AV')
INSERT [dbo].[SanPham] ([MaSP], [TenSP], [Gia], [MoTa], [Hinh], [LoaiId]) VALUES (N'FT01', N'Trà đen đào', 25000.0000, N'healthy ', N'Đen-đào-2.png', N'TS')
INSERT [dbo].[SanPham] ([MaSP], [TenSP], [Gia], [MoTa], [Hinh], [LoaiId]) VALUES (N'FT02', N'Trà bí đao', 25000.0000, N'healthy x2', N'Trà-Bí-Đao-Milkfoam-2.png', N'FT')
INSERT [dbo].[SanPham] ([MaSP], [TenSP], [Gia], [MoTa], [Hinh], [LoaiId]) VALUES (N'FT03', N'Trà Oolong vải', 25000.0000, N'healthy x3', N'Oolong-vải-2.png', N'FT')
INSERT [dbo].[SanPham] ([MaSP], [TenSP], [Gia], [MoTa], [Hinh], [LoaiId]) VALUES (N'FT04', N'Trà tắc', 25000.0000, N'healthy x4', N't8.jpg', N'FT')
INSERT [dbo].[SanPham] ([MaSP], [TenSP], [Gia], [MoTa], [Hinh], [LoaiId]) VALUES (N'FT05', N'Trà xanh đào', 25000.0000, N'healthy x4', N'Xanh-đào-2.png', N'FT')
INSERT [dbo].[SanPham] ([MaSP], [TenSP], [Gia], [MoTa], [Hinh], [LoaiId]) VALUES (N'TP01', N'Pudding', 5000.0000, N'Perfect x2', N'布丁-pudding.png', N'TP')
INSERT [dbo].[SanPham] ([MaSP], [TenSP], [Gia], [MoTa], [Hinh], [LoaiId]) VALUES (N'TP02', N'Thạch trái cây', 5000.0000, N'Perfect ', N'Thạch-trái-cây.png', N'TP')
INSERT [dbo].[SanPham] ([MaSP], [TenSP], [Gia], [MoTa], [Hinh], [LoaiId]) VALUES (N'TP03', N'Thạch vị dâu', 5000.0000, N'ngon x3', N'Tc-trang-vi-dau.png', N'TP')
INSERT [dbo].[SanPham] ([MaSP], [TenSP], [Gia], [MoTa], [Hinh], [LoaiId]) VALUES (N'TP04', N'Trân châu đen', 5000.0000, N'dỡ ẹt', N'Trân-Châu-Đen.png', N'TP')
INSERT [dbo].[SanPham] ([MaSP], [TenSP], [Gia], [MoTa], [Hinh], [LoaiId]) VALUES (N'TP05', N'Trân châu đen', 5000.0000, N'ngon hơn trân châu đen', N'Trân-Châu-Trắng.png', N'TP')
INSERT [dbo].[SanPham] ([MaSP], [TenSP], [Gia], [MoTa], [Hinh], [LoaiId]) VALUES (N'TS01', N'Trà sữa truyền thống', 30000.0000, N'ngon', N'Hinh-Web-OKINAWA-TRÀ-SỮA.png', N'TS')
INSERT [dbo].[SanPham] ([MaSP], [TenSP], [Gia], [MoTa], [Hinh], [LoaiId]) VALUES (N'TS02', N'Trà sữa sicula', 30000.0000, N'ngon x2', N'Trà-sữa-Chocolate-2.png', N'TS')
INSERT [dbo].[SanPham] ([MaSP], [TenSP], [Gia], [MoTa], [Hinh], [LoaiId]) VALUES (N'TS03', N'Trà sữa Coffee', 30000.0000, N'ngon x3', N'Okinawa-Coffee-Milk-Tea.png', N'TS')
INSERT [dbo].[SanPham] ([MaSP], [TenSP], [Gia], [MoTa], [Hinh], [LoaiId]) VALUES (N'TS04', N'Trà sữa xoài', 30000.0000, N'ngon x3', N'Mango-Milktea.png', N'TS')
INSERT [dbo].[SanPham] ([MaSP], [TenSP], [Gia], [MoTa], [Hinh], [LoaiId]) VALUES (N'TS05', N'Trà sữa Matcha', 30000.0000, N'ngon x5', N'ts7.jpg', N'TS')